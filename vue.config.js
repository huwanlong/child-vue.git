module.exports = {
  configureWebpack: {
    output: {
      library: 'singleVue',
      libraryTarget: 'umd'
    },
    devServer: {
      port: 10001
    }
  }
}

// window.singleVue.bootStap / mount / unmount
